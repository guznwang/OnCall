﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace 命令处理机
{
    public class Command
    {
        /// <summary>
        /// 重定向输入执行命令
        /// </summary>
        /// <param name="cmds"></param>
        /// <returns></returns>
        public string ExecuteByInput(string[] cmds)
        {
            string output = "";
            Process process = new Process();
            ProcessStartInfo procInfo = new ProcessStartInfo();
            procInfo.FileName = "cmd.exe";
            procInfo.UseShellExecute = false;
            procInfo.RedirectStandardInput = true;
            procInfo.RedirectStandardOutput = true;
            procInfo.CreateNoWindow = true;
            process.StartInfo = procInfo;
            try
            {
                process.Start();
                process.StandardInput.WriteLine("@echo off");
                foreach (string cmd in cmds)
                {
                    process.StandardInput.WriteLine(cmd);
                }
                process.StandardInput.WriteLine("exit");
                process.WaitForExit();
                output = process.StandardOutput.ReadToEnd();
            }
            catch
            { }
            process.Dispose();
            return output;
        }

        /// <summary>
        /// 重定向输入执行命令
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public string ExecuteByInput(string cmd)
        {

            string output = "";
            Process process = new Process();
            ProcessStartInfo procInfo = new ProcessStartInfo();
            procInfo.FileName = "cmd.exe";
            procInfo.UseShellExecute = false;
            procInfo.RedirectStandardInput = true;
            procInfo.RedirectStandardOutput = true;
            procInfo.CreateNoWindow = true;
            process.StartInfo = procInfo;
            try
            {
                process.Start();
                process.StandardInput.WriteLine("@echo off");
                process.StandardInput.WriteLine(cmd);
                process.StandardInput.WriteLine("exit");
                process.WaitForExit();
                output = process.StandardOutput.ReadToEnd();
            }
            catch
            { }
            process.Dispose();
            return output;
        }

        /// <summary>
        /// 启动参数输入执行命令
        /// </summary>
        /// <param name="cmds"></param>
        /// <returns></returns>
        public string ExecuteByArgs(string[] cmds)
        {
            string output = "";
            ProcessStartInfo procInfo = new ProcessStartInfo();
            Process process = new Process();
            procInfo.FileName = "cmd.exe";
            procInfo.Arguments = "/c " + string.Join("&", cmds);
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            procInfo.RedirectStandardOutput = true;
            process.StartInfo = procInfo;
            try
            {
                process.Start();
                process.WaitForExit();
                output = process.StandardOutput.ReadToEnd();
            }
            catch
            { }
            process.Dispose();
            return output;
        }

        /// <summary>
        /// 启动参数输入执行命令
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public string ExecuteByArgs(string cmd)
        {
            string output = "";
            ProcessStartInfo procInfo = new ProcessStartInfo();
            Process process = new Process();
            procInfo.FileName = "cmd.exe";
            procInfo.Arguments = "/c " + cmd;
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            procInfo.RedirectStandardOutput = true;
            process.StartInfo = procInfo;
            try
            {
                process.Start();
                process.WaitForExit();
                output = process.StandardOutput.ReadToEnd();
            }
            catch
            { }
            process.Dispose();
            return output;
        }

        /// <summary>
        /// 执行单行指令
        /// </summary>
        /// <param name="info"></param>
        public static void ExecuteSingleCommand(CommandInfo info)
        {
            Command cmd = new Command();
            cmd.ExecuteByArgs(string.Join(" ",info.Args));
        }

        /// <summary>
        /// 执行多行指令
        /// </summary>
        /// <param name="info"></param>
        public static void ExecuteMultiCommand(CommandInfo info)
        {
            Command cmd = new Command();
            cmd.ExecuteByInput(info.Args[0]);
        }

        /// <summary>
        /// 关机
        /// </summary>
        /// <param name="info"></param>
        public static void PowerOff(CommandInfo info)
        {
            Command cmd = new Command();
            cmd.ExecuteByInput("shutdown -s -t 10");
        }

        /// <summary>
        /// Shutdown命令封装
        /// </summary>
        /// <param name="info"></param>
        public static void Shutdown(CommandInfo info)
        {
            Command cmd = new Command();
            if (info.Args.Length > 0)
            {
                cmd.ExecuteByInput("shutdown " + string.Join(" ", info.Args));
            }
            else
            {
                cmd.ExecuteByInput("shutdown -s -t 1");
            }
        }

        /// <summary>
        /// 重启
        /// </summary>
        /// <param name="info"></param>
        public static void ReStart(CommandInfo info)
        {
            Command cmd = new Command();
            cmd.ExecuteByInput("shutdown -r -t 10");
        }

        /// <summary>
        /// 终止Shutdown命令
        /// </summary>
        /// <param name="info"></param>
        public static void ShutdownCanCel(CommandInfo info)
        {
            Command cmd = new Command();
            cmd.ExecuteByInput("shutdown -a");
        }
    }
}
