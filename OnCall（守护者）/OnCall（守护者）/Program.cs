﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using 前台界面;
using 命令解释器;
using 邮件访问内核;

namespace OnCall
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                MailBoxShellInfo shellInfo = new MailBoxShellInfo();
                MailBoxAccessShell mailBoxShell = new MailBoxAccessShell(shellInfo);
                CommandInterpreter interpreter = new CommandInterpreter();
                MainWindow window = new MainWindow(mailBoxShell);
                ComponentLinker linker = new ComponentLinker(window, mailBoxShell, interpreter);
                linker.Link();

                //运行窗体
                Application.Run(window);
            }
            catch(Exception ex)
            {
                Log(ex);
            }
        }

        static void Log(Exception ex)
        {
            FileStream logger = new FileStream("LYC.log", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(logger);
            writer.WriteLine("---------- {0} ----------", DateTime.Now);
            writer.WriteLine("Source:{0}", ex.Source);
            writer.WriteLine("TargetSite:{0}", ex.TargetSite);
            writer.WriteLine("Message:{0}", ex.Message);
            writer.WriteLine("HelpLink:{0}", ex.HelpLink);
            writer.WriteLine("");
            writer.Flush();
            writer.Close();
            logger.Close();
            MessageBox.Show("程序遇到异常导致中断，异常信息已写入记录文件。" + System.Environment.NewLine + "欢迎向我报告错误：saber000@vip.qq.com。", "提示 @刘奕聪", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
