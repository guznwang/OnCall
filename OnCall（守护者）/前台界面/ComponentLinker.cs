﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using 命令解释器;
using 命令处理机;
using 邮件访问内核;

namespace 前台界面
{
    /// <summary>
    /// 组件连接器
    /// </summary>
    public class ComponentLinker
    {
        private MainWindow window;
        private MailBoxAccessShell mailBoxShell;
        private CommandInterpreter interpreter;
        private Thread interpretThread;

        public ComponentLinker(MainWindow w, MailBoxAccessShell m, CommandInterpreter i)
        {
            window = w;
            mailBoxShell = m;
            interpreter = i;
        }

        /// <summary>
        /// 连接组件
        /// </summary>
        public void Link()
        {
            CommandsResgiter();
            ShellInfoConfiguration();
            EventLinker();
            DebugHelper();
        }

        /// <summary>
        /// 命令注册
        /// </summary>
        public void CommandsResgiter()
        {
            interpreter.Register("EXEC", Command.ExecuteSingleCommand);
            interpreter.Register("#EXEC", Command.ExecuteMultiCommand);
            interpreter.Register("SHUTDOWN", Command.Shutdown);
            interpreter.Register("POWEROFF", Command.PowerOff);
            interpreter.Register("关机", Command.PowerOff);
            interpreter.Register("RESTART", Command.ReStart);
            interpreter.Register("重启", Command.ReStart);
            interpreter.Register("取消终止命令", Command.ShutdownCanCel);

            interpreter.Register("MSGBOX", 系统相关.系统提示);
            interpreter.Register("提示", 系统相关.系统提示);
            interpreter.Register("#MSGBOX", 系统相关.系统提示);
            interpreter.Register("#提示", 系统相关.系统提示);
            interpreter.Register("START", 系统相关.启动程序);
            interpreter.Register("启动", 系统相关.启动程序);
            interpreter.Register("LOCK", 系统相关.锁闭系统);
            interpreter.Register("锁闭系统", 系统相关.锁闭系统);
            interpreter.Register("SWITCH", 系统相关.显示屏开关);
            interpreter.Register("关屏幕", 系统相关.显示屏开关);

            interpreter.Register("TEST", Test.test);
        }

        /// <summary>
        /// 邮件访问内核信息配置
        /// </summary>
        public void ShellInfoConfiguration()
        {
            MailBoxShellInfo shellInfo = mailBoxShell.shellInfo;
            shellInfo.Delay = 200;
            shellInfo.Frequency = 300;
            shellInfo.Port = 110;
            shellInfo.Feature = "LLL";
            shellInfo.DeleteWhenFinished = true;
        }

        /// <summary>
        /// 事件连接
        /// </summary>
        public void EventLinker()
        {
            mailBoxShell.InstructionArrived += new InstructionArrivedEventHandler(window.InstructionArrived);
            mailBoxShell.InstructionArrived += new InstructionArrivedEventHandler(mailBoxShell_InstructionArrived);

            mailBoxShell.NewMailArrived += new NewMailArrivedEventHandler(window.mailBoxShell_NewMailArrived);
        }

        /// <summary>
        /// debug设置
        /// </summary>
        private void DebugHelper()
        {
        }

        /// <summary>
        /// 指令送入解释器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="mail"></param>
        public void  mailBoxShell_InstructionArrived(object sender, LumiSoft.Net.Mail.Mail_Message mail)
        {
            interpreter.Interpret(mail.BodyText);
        }
    }
}
